{\rtf1\ansi\ansicpg1252\cocoartf1561\cocoasubrtf600
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww15140\viewh14980\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 # Python Lecture - Trees\
\
Another way to store data, essentially a series of nodes.\
Trees model hierarchical or nested structure\
Trees consist of points called nodes and connections between them called edges\
One node is special, called the root\
Every node n other than the root is connected by an edge to another node called the parent of n \
\
Each node has only one parent \
Trees are connected - starting with any node n other than the root you can reach the root by following the parents recursively\
If node p is the parent of node c\
Parent child relationships extends to ancestors and descendants\
Subtree - a node together with all its descendants is a subtree\
A leaf is a node that has no children, an interior node has one or more children\
\
## Expression trees \
If a node is an operator then the operation is completed further down the descendants to be able to complete the higher up expression\
\
## Binary Trees\
Each node has at most 2 children (left and right)\
\
## Binary Search Tree\
The evaluation on the left is less than the evaluation on the right\
\
## Big O \
Creating the tree is logorithmic\
Accessing a created tree is linear dependent on the number of nodes\
\
## States of tree\
There are three states of tree\
Full - All leaves are at the same level \
Complete - Where all levels of the tree are filled except possibly the last level and if the last level isn\'92t filled, the all of its leaves are as far to the left as possible\
Balanced - No leaf is much further away from the root than any other leaf. Balanced branch trees are a lot more efficient when performing search and insertion.\
\
## Deleting Nodes:\
Replace the interior node with one of its leaf nodes - the highest value on the lowest side\
\
## Sets and set methods\
Implement sets using this method using a binary tree\
Make yourself a set using binary trees\
Sets - like unordered arrays, a way of storing data, can be a string or a tuple e.t.c.\
\
## AVL Trees \
A lot more logic intensive and difficult to implement \
Self-balancing Binary Search tree\
Balancing performed after insertion or deletion causes the tree to become \'91unbalanced\'92\
\
If tree is balanced if the heights of the nodes differ by at most 1\
\
## AVL Balance Rule\
Heights of left and right children must differ by <= 1\
To restore balance - rotate \
\
### Left-Right Case\
First left-rotation y\
Then right rotate on x\
\
Symmetric goes for a Right-Left Case\
\
Look up red black tree\
\
## Whats the point? \
Whilst its much more complex to create and maintain than a standard array, in terms of Big O it is a lot more time efficient to perform operations on a tree than an array.p0\
}