## DRAM Physical Memory
Bits stored as charge in tiny capacitors
The charge slowly leaks so must be refreshed periodically, hence 'Dynamic'
Random Access (contrast Sequential Access)
Byte or Word Addressable
Cached on CPU per core, multi-level these days
Volatile - data disappears when powered off

## Latency numbers every programmer should know - 
Memory is 100,000 times faster than discs
But discs have alot of space and are really cheap
Need to manage the space between disc and Memory

## Concepts
Every process needs memory for:
Instructions ("text")
Static data (known as compile time)
Dynamic data (heap and stack)

## Relocation
Programmer cannot know addresses that process will occupy at runtime
Kernel may want to swap processes into and out of memory to maximise CPU utilisation
Inefficient to require a swapped-in process to always go to the same place in memoryProcesses incorporate addressing info (instruction branches, pointers)
OS must manage these references to make sure they are sane
Thus need to translate logical to physical addresses

## Protection
Protect each process from others
Read, write execute permissions to smaller chunks of memory
Computed addresses (like indexing arrays) should be sanity checked

## Address binding solutons:
Compile Time - requires knowledge of absolute addresses - MS-DOS worked this way, not suitable for multiprocessing
Load Time - update addresses in program after loading, when physical addresses become known.
Run time - 

## Virtual Memory
The same physcal page can be mapped on more than one virtual page
If the virtual pages are in different process, they share memory
This mechanism is used to share library code 
Also be used to share data between processes
