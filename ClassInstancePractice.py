#Creating a student class for Hogwarts

class Student:

    #making a class variable for the year change
    yearchange = 1

    #setting a class counter for number of students
    num_of_stus = 0

    #self is referring to the instance
    def __init__(self, first, last, house, pet, year):
        # allows you to automatically set later on, same as student.first = Hermione 
        self.first = first
        self.last = last
        self.house = house

        if pet == None:
            self.pet = None
        elif pet != None:
            self.pet = pet 
        
        self.year = year

        Student.num_of_stus += 1 
    
    @property
    def fullname(self):
        return '{} {}'.format(self.first, self.last)

    # now can call first and last name seperately
    @fullname.setter
    def fullname(self, name):
        first, last = name.split(' ')
        self.first = first
        self.last = last 
    
    @fullname.deleter
    def fullname(self):
        print('Delete Name!')
        self.first = None
        self.last = None
    
    @property
    def petfull(self):
        return '{} {}'.format(self.pet, self.last)

    def year_change(self):
        #need self or employee in front of yearchange
        #year_change is creates as an attribute for each of the students
        self.year = int(self.year + self.yearchange)
    
    @classmethod
    # can't use the word class as the variable name as it has a different meaning within the language 
    def set_year_change (cls, amount):
        cls.yearchange = amount 

    
    def __repr__(self):
        return "Student('{}', '{}', '{}')".format(self.first, self.last, self.house)
    
    def __str__(self):
        return '{} - {}'.format(self.fullname(), self.pet)
    
    def __add__(self, other):
        return self.year + other.year
    
    def __len__(self):
        return len(self.fullname())
    

#putting student as a n arguments means we inherit all its functionality and makes it a subclass
class QuidditchCaptain(Student):
    def __init__(self, first, last, house, pet, year, position):
        #need this part to set the attribute which is not related to the other instance methods of the overall class 
        #subclass init method
        super().__init__(first, last, house, pet, year)
        self.position =  position


Student.set_year_change(2)
print(Student.num_of_stus)
stu_1 = Student('Hermione', 'Granger', 'Gryffindor', 'Crookshanks', 3)
stu_2 = Student('Luna', 'Lovegood', 'Ravenclaw', None, 2)
Quid_1 = QuidditchCaptain('Oliver', 'Wood','Gryffindor', None, 6, 'Keeper')
Quid_2 = QuidditchCaptain('Cedric', 'Diggory', 'Hufflepuff', None, 5, 'Seeker')
print(Student.num_of_stus)

print (stu_1.pet)
print (stu_2.pet)

#manually getting first and last names
print('{} {}'.format(stu_1.first, stu_1.last))

#getting first and last names using a the fullname method
#need the brackets becuase its a method?

#when we call the method on the instance it passes in self automatically
print(stu_1.fullname())

#when we call the method on the class it doesn't know which instance you are referring to 
print(Student.fullname(stu_1))

#changing the year 
print(stu_1.year)
stu_1.year_change()
print(stu_1.year)

print(Quid_1.pet)
print(Quid_1.position)

print(repr(stu_1))
print(str(stu_1))

#prints the added pay, dont have to specify method
print(stu_1 + stu_2)