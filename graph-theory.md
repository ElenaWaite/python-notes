# Introduction to Graphs

## What is a graph?
Points between nodes
Unweighted/weighted - whether each edge has a weight
Directed/undirected - whether edges have a direction
Vertex labeled - vertexes can hold information
Edge labeled - edges can hold information, labelling the nodes
Cyclic/acyclic - whether there are cycles in the graph
Connected/disconnected - whether the graph is one big component or not 

Any kind of data structure is osme kind of graph, but has very specific way for you to interact with it - e.g. trees

## 3 methods for representing graphs:
Edge List - the data structure describing the graph AB
Adjacency List - A:B
Adjacency Matrix - 01

## Graph Traversal: DFS
DFS(node X)
1. Check if node X has been traversed. If traversed stop.


## Use cases: DFS - Depth First Search
Finding connected components in a graph
Topological sort
Finding bridges in a graph
Finding strongly connected components
Maze solving
Cycle Detection

## Graph Traversal: BFS - Breadth First Search
A weighted undirected graph, tell which 

## Dijkstras Algorithms
Generate an minimal distance map where each node is infinite distance away
Make beginning node distance
While ending node/all nodes have not been traversed
- Pick the node with the least distance from the beginning node
- For every non-traversed neighbor of current node
 - Calculate distance to it using the triangle rule:
 Dist(neigbour) = min (Dist(neigbour)), Dist(currentNode) + edge(currentNode, neighbour)

