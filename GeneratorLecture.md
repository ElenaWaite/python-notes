# Generators
They are like functions but they yield rather than return values, and maintain their state. Calling a generator function returns a generator object.\
Any python function with a yield function in its body is a generator function which, when called, returns a generator object. In OO language, it is a generator factory.\
Generators are iterators that produce the values passed to yield\
If g is a generator, its also an iterator, so next (g) fetches the next item produced by yield
Generators are lazy, they produce one item at a time 
Built in lambda functions for generating - 

- filter() - take a generator and only return numbers in the data is consuming that pass certain set criteria
- map() - applies a function to everything in a sequence and return a new sequence 
- Enumerate () - returns tuples containing elements and their indices.
- Accumulate () - keep a running sum 
- iter() - can use it with a regular function, it will call it repeatedly until it gets a terminating value out of the call
- itertools.accumulate() - apply a function to each element and the cumulative result of earlier applications
- Itertools.chain() - make a sequence out of several
- zip() - return tuples containing the next elements of more than one sequence

Sentinals - special values which you take a different action at when you reach them 

# Coroutines
Are similar to generators with a few differences
Main difference:
Generators are data producers
Coroutines are data consumers

They are fed the value of yield externally 

# Abstract Data Types\
Finite sequence of zero or more elements\
Can represent vectors, lists have length, first element called head, remainder tail
Elements have position 
A list or a sequence is an abstract data type that represents a countable number of ordered values

## Linked-List Data Structure\
A linked list is a way to store data with an order
Start by thinking about an array, a fixed chunk in computer memory, this chunk is then subdivided into smaller sections, which can be accessed using indices, starting with the index 0.
Can access in constant time - O(1) 

The linked list has no strict linear ordering in memory, instead the elements are controlled by each individual data structure having its own element,  the linked list uses a data structure called a node to hold its data together, this node also includes the metadata which sows the linked list together.

The node contains a pointer to the next element in the list. The last node can be identified as its pointer is set to null.\

Single Linked List - pointers to the next element
Double Linked List - pointers to the prior and next elements 

## Queue:
FIFO queue - the first tasks added are the first received 
LIFO queue - the most recently added entry is the first retrieved 

### Classes and exceptions of the queue module:
Class Queue.Queue(maxsize=0)
Constructor for a FIFO queue. Maxsize is an integer that sets the upperbound limit on the number of items that can be placed in the queue. Insertion will block once this size has been reached, until queue items are consumed. If i maxsize i0  is less than or equal to zero, the queue size is infinite.

### Class Queue.LifoQueue(maxsize=0) 

#### Constructor for a LIFO queue. 
maxsize is an integer that sets the upperbound limit on the number of items that can be placed in the queue. Insertion will block once this size has been reached, until queue items are consumed. If i maxsize i0  is less than or equal to zero, the queue size is infinite.


#### PriorityQueue
maxsize=0
Constructor for a priority queue. maxsize is an integer that sets the upperbound limit on the number of items that can be placed in the queue. Insertion will block once this size has been reached, until queue items are consumed. If maxsize is less than or equal to zero, the queue size is infinite.\cb1 \
The lowest valued entries are retrieved first (the lowest valued entry is the one returned by 
sorted(list(entries))[0]
 A typical pattern for entries is a tuple in the form: 
(priority_number, data)


#### Exception 
Queue.Empty
Exception raised when non-blocking get() or get_nowait() is called on a Queueobject which is empty.

#### Exception 
Queue.Full
Exception raised when non-blocking put() or put_nowait() is called on a Queue object which is full.