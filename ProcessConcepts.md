#Process Concepts
## Process is different from a program
- Program is static and on disk 
- Process is dynamic, a program in execution 

## What happens when we run a python program?
Steps performed by the Python Interpretor which is itself a program:
1. Lexing - lexical analysis step splits the sequence of characters into tokens, what in ordinary language we call words)
2. Parsing (and semantic analysis) - understands the tokens emitted by the lexing phase according to python's grammar, and builds a data structure called an Abstract Syntax Tree.
3. Compiling - producing python bitecode from the AST.
4. Running - load the bytecode into the Python virtual machine where it is then run.

## Python's Virtual Machine 
Stack-based - three types
Call stack - main structure of the running program, with one item (frame) for each currently active function call. The bottom of the call stack is the entry point for the program.
Evaluation stack - each call stack frame has a data stack where data is pushed, manipulated and popped.
Block stack - used to keep track of control structures like loops, try/except

## Programs written in other languages 
Not all programs generate bytecode
Java compiles source code to bytecode for the Java VM.
Traditional compiled languages compile their source code to native machine code and emit the code as a file in a runnable format. The C, C++ and Go languages work this way.

## Processes from the operating system perspective
Each process is a - 
- Unit of resource allocation (memory, files, e.t.c.)
- Unit of protection - by default processes are isolated from other running processes and permissions are enforced
Multiple copies of the same program can run simultaneously
Processes run on virtual processors
Each process has its own virtual address space
One or more threads, each with: program counter, stack, data

Program can't effect the memory of other programs

## Process States
New (being created)
Running - CPU executing process code
Ready to run - waiting on run queue for CPU
Sleeping (blocked) - waiting for an event
Stopped by job control or debugger
Defunct(zombie) - finished running, but waiting for parent to harvest return code

## Process Creation
Processes are hierarchical like trees
Processes have unique integer ids (process id = pid)
The root procees is special, hamd-crafted when the system boot.
In every other case, parent processes create child processes with a fork() system call.

fork() creates a clone of the parent process, including open files
- parent process creates child process
child recieves copy-on-write (COW) snapshot of parents address space

Parent typically either:
detaches from child - hands responsibility back to init process
waits for child - callling wait(), parent blocks until child exits

## Process Termination
Normal termination by calling exit() 
Child's exit status

## System Calls and blocking
Control is transferred to the kernel to satisfy the request
Happens with things like retrieving file
The OS blocks a process why the system call is completed, the system call is required for the process

Processes spend most their time waiting for output or input.


