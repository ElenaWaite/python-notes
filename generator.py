#Generators

#this format would have been easier to do a list comprehension
#generators use yield
def square_numbers(nums):
    for i in nums:
        yield (i*i)

my_nums = square_numbers([1,2,3,4,5])

# will not print any value
print(my_nums)

# will print one value
print('printing next:')
print(next(my_nums))

#will print all the values
#in this case it prints everything but the first as that was allready generated above
for x in my_nums:
    print('printing iteration:')
    print(x)


# why to use a generator over a list comprehension
#loop through the number of people passed in
def people_generator(num_people):
    for i in xrange(num_people):
        #yield this dictionary
        person = n{
                    'id:': i 
                    'name': random.choice(names),
                    'major': random.choice(majors)
        }
        yield person




