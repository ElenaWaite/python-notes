
#user will never interact with the node class, binary search tree class will be a wrapper which deals with the node class
class node:
    #set a value to none 
    def __init__(self, value=None):
        #save that value
        self.value=value
        #2 pointers for each side of the tree
        self.left_child=None
        self.right_child=None
    
class binary_search_tree:
    def __init__(self):
        #set internal variable for root, no elements yet so set to none
        self.root=None
    
    #allowing you to add elements to the search tree
    def insert(self, value):
        #ask if we can insert value at root, if theres nothing there already then we can
        if self.root==None:
            #pass in the value from the node class, save internally
            self.root=node(value)
        else:
            #essentially a private function, should refrain from calling outside of class (_)
            #private insert function is recursive 
            #hndling the recursion in a private function is easier than having it in the main function
            self._insert(value, self.root)
    
    def _insert(self, value, cur_node):
        #if the value we want to insert is less than the value in the current node
        if value<cur_node.value:
            #and if the current value doesn't have a left child 
            if cur_node.left_child==None:
                #create a new node and set the left child of the current node equal to that new node
                cur_node.left_child=node(value)
            else:
                #if the current node does have a left child, then we are calling the recursive function
                # there we pass the current nodes left child as the current node 
                # here we have recursion down the left part of the tree
                self._insert(value,cur_node.left_child)
        # if the value is greater than the current node, then same for right side
        elif value>cur_node.value:
            if cur_node.right_child==None:
                cur_node.right_child=node(value)
            else:
                self._insert(value,cur_node.right_child)

        else:
            print ("Value already in tree!")

    
    def print_tree(self):
        if self.root!=None:
            self._print_tree(self.root)

    def _print_tree(self,cur_node):
        if cur_node!=None:
            self._print_tree(cur_node.left_child) 
            print (str(cur_node.value))
            self._print_tree(cur_node.left_child)  

    def height(self):
        if self.root !=None: 
            return self._height(self.root,0)
        else:
            return 0

    def _height(self,cur_node,cur_height):
        if cur_node==None: 
            return cur_height
        #need to return current height + 1 so the recursive tabulates right
        left_height=self._height(cur_node.left_child,cur_height+1)
        right_height=self._height(cur_node.right_child,cur_height+1)

        #whichever one is larger, left or right subtree height will be the one that is returned
        return max(left_height, right_height)

    def search(self,value):
        if self.root!=None:
            return self._search(value,self.root)
        else:
            return False
    
    def _search(self,value,cur_node):
        if value==cur_node.value:
            return True
        elif value<cur_node.value and cur_node.left_child!=None:
            #returning this allows you to report accurately what is in the node
            return self._search(vaue,cur_node,left_child)
        elif value>cur_node.value and cur_node.right_child!=None:
            return self._search(value,cur_node.right_child)
        return False

            
def fill_tree(tree, num_elems=100, max_int=1000):
    from random import randint
    for _ in range(num_elems):
        cur_elem = randint(0, max_int)
        tree.insert(cur_elem)
    return tree

tree = binary_search_tree()

#tree = fill_tree(tree)
tree.insert(5)
tree.insert(10)
tree.insert(15)
tree.insert(20)
tree.insert(25)
tree.insert(30)
tree.insert(35)

tree.print_tree()

print ("tree height: " + str(tree.height()))

print (tree.search(10))
print (tree.search(33))