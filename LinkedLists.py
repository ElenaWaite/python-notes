#sub-class, user does not interact with it 
class node:
    def __init__(self,data=None):
        self.data=data  
        self.next=None

# wrapper class
class linked_list:
    def __init__(self):
        # this is a pointer so we can point to the first element in the list
        self.head=node()

    #adds new nodes containing data to the end of the linked list
    def append(self,data):
        new_node = node(data)
        cur = self.head
        while cur.next!=None:
            cur = cur.next 
        cur.next = new_node
    
    #returns the length (integer) of the linked list
    def length(self):
        cur = self.head
        total = 0 
        #once this total iteration is complete
        while cur.next!=None:
            total+=1
            cur = cur.next
        return total 
    
    # prints put the linked list in traditional python list format 
    def display(self):
        elems = []
        cur_node = self.head
        while cur_node.next!=None:
            cur_node=cur_node.next
            elems.append(cur_node.data)
        print (elems)
    
    #returns the value of the node at 'index'
    def get(self,index):
        if index>=self.length() or index<0: # added 'index<0' post-video 
            print("ERROR: 'Get' Index out of range!")
            return None
        cur_idx=0
        cur_node=self.head
        while True:
            cur_node=cur_node.next
            #if current node is at the indx the user entered
            if cur_idx==index: return cur_node.data
            cur_idx+=1
    
    #delete the node at index 'index'
    def erase(self,index):
        if index>=self.length() or index<0: # added 'index<0' post-video 
            print("ERROR: 'Erase' Index out of range!")
            return 
        cur_idx=0
        cur_node=self.head         
        while True:
            last_node=cur_node
            cur_node=cur_node.next
            if cur_idx==index:
                last_node.next=cur_node.next
                return
            cur_idx+=1

    # Allows for bracket operator syntax (i.e. a[0] to return first item).
    def __getitem__(self, index):
        return self.get(index)


my_list = linked_list()

my_list.display()

my_list.append(1)
my_list.append(2)

my_list.display()

print (my_list.get(1))
my_list.erase(0)
my_list.display()
    
        


