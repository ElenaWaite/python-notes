
## Chapter 1 Getting Started

### Knowledge:
Either Declarative or imperative.

### Declarative: statements of fact.
Imperative: how to knowledge, recipes for deducing information. 

### Programming:
Describing a computation so it can be done mechanically

### Algorithm:
Form of imperative knowledge.
An algorithm is a finite list of instructions that describe a computation that when executed on a set of inputs will proceed through a set of defined states and eventually produce and output.

Shift from fixed programme computers that did one thing to stored program computers that can do multiple.
Example of fixed programme computer – calculator.

### Interpreter:
Heart of the computer can execute any legal set of instructions, can be used to compute anything that one can describe using a basic set of instructions

### Flow of control:
The interpreter jumping to a different point in the computer’s memory based on the output of a set of instructions
Essential for writing programs that perform complex tasks

### Turing
Not all programs have computational solutions – Church-Turing thesis
Universal Turing Machine – if a function if computable, then this machine could be programmed to compute it 
A programming language is Turing complete if it can be used to simulate the Universal Turing machine – calculate or the things that are computable 
All programming languages are Turing complete from Python to Java
So, anything programmed on one can be programmed on another although it may be easier on one

Python is a good ‘general purpose’ language

### Abstraction
No one actually knows completely how a computer works as it works at many different levels

### Programming languages have a set of:
Primitive constructs: words (English)
Syntax: what a valid construction is in the language, described which strings of words constitute well-formed sentences, whether it makes sense in the programming language, actually means something. You cannot execute a program with a syntactic error.
Static semantics: which sentences are meaningful
Semantics: associate meaning with syntactically correct strings, defines meaning of those sentences, each meaningful section of code tells the computer one thing only, there is no ambiguity like in natural languages. Whilst you don’t normally get to the point of having semantic errors, the semantics of a program may be different to what was intended.



### 3 options for programmes which run in an unintended way:
Crash – stop running and indicate that it has done so, may damage the entire system however.
Keep running and not stop – can be hard to recognise if you don’t know how long it should be.
Run to completion and produce an answer that might not be correct

The last is the worst – if a program appears to doing the right thing but isn’t it is hard to identify and can have escalating negative consequences. 

Programs should be written in such a way that when they don’t work properly it is self-evident.

## Chapter 2 Lecture and Notes - Introduction to Python

### 3 ways that programs are the most related:

#### Low level vs high level:
Refers to whether we program to the level of the machine or using abstract operations e.g. a pop up menu, provided by the language designer

#### General vs targeted to an application domain
Whether the applications of the programming language can be widely used or not

#### Interpreted versus compiled
Whether the sequence of instructions written by the programmer called source code executed directly by an interpreter or converted by a compiler into a sequence of machine-level primitive operations.
There are different advantages to both, eager to debug interpreted languages, but compiled programs run more quickly and use less space

#### Python
General-purpose programming language
Not good for programs that have high reliability constraints or need to be maintained by many people over a long period of time – because it has weak semantic checking
Easy to learn
Designed to be interpreted so has a quick run time for new users
Large number of freely available libraries
Continuing support and development of the python ecosystem and its libraries has become a collective activity 
Not backward compatible – programs run on Python 2 need to be converted before they can be used on Python 3 and 3.5

### Basic elements of Python
Objects, Expressions and Numerical Types

### Objects
Core things a python language can manipulate
Either scalar or non-scalar
Scalar objects are indivisible
Non-scalar objects – like strings, have internal structure
Many types of objects are represented by literals in the text of the program – text 2 is a literal representing a number and the text ‘abc’ a literal representing a string

### Four types of scalar objects:
To find out what object something is – type(the thing) 
int 
represents integers, counting numbers (5)

#### float 
represent real numbers, include a decimal point (3.0), stands for floating point numbers.

#### bool 
used to represent the Boolean values True and False
 primitive operators on type bool are: and, or, not

#### a and b – is true if both a and b are true, and false otherwise
a or b is true if at least one of a and b is true, and false otherwise
not a is true if a is false and false if a is true

#### none 
a type with a single value

Objects and operators (+, = e.t.c.) combine to form expressions

### Operators:

== operator used to test whether two expressions evaluate to the same value
!= operator used to measure whether two expressions evaluate to different values
= 1 equals symbol associates the name to the left of the symbol with the object donated on the right

>>> shell prompt indicating the interpreter is expecting the user o type some code

Arithmetic operators have the same precedence e.g. * binds more tightly than +, order of evaluation is changed by adding ()

** = raising to a power
Power of 2 = **2 

+ concatenation when applied to two strings, addition when applied to two numbers

### Variable and Assignment
Variables
Variables provide a way to associate names with objects
An object can have one, more than one, or no name associated with it
To python it is just a name
Case sensitive
Small number of reserved words that have built in meanings and can’t be used for variables, these are – and, as, assert, break, class, continue, def, del, elif, else, except, False, finally, for, from, global, if, import, in, is, lambda, nonlocal, None, not, or, pass, raise, return, True, try, while, with and yield
Labelling things is good, helps you keep track 

### You can swap the bindings of two variables – 
x = 1
y = 2
x, y = y, x
x = 2
y = 1


### Branching Programs
Some code runs one statement after another – straight line programs
Branching programs don’t do this, simplest one is a conditional/ an if-else block

### Three parts to a conditional statement:
A test i.e. an expression that evaluates either True or false
A block of code that runs if true
An optional block of code that runs if false

Boolean expression – any expression that evaluates as true or false

Indentation
Semantically meaningful 
True and false blocks of code need to be nested

4 spaces indentation 

Use indentation to put blocks inside blocks

Strings and Input

Back slash – ignore a character or don’t give special meaning to the character that follows it
Alternatively – double quotes rather than single quotes avoid significance

If you need a backslash – use two, one will create an error

Adding strings together – join parts of a sentence
Multiply strings, the string is repeated a certain number of times

Start counting with 0 not with 1 



Storing programs that you don’t want to type out again and again in program files.

In shell – line of code telling the shell to run it in python 

Set – data structure which holds one of each kind of element you put in it 
 Integer – counting number
When you divide to integers you always get a floating point numbers back regardless even if it is a whole number (.0)
Dividing with two lines returns an integer if that is possible
 
If you do mixed math between floating point numbers and integers it gets promoted to floating point numbers

Floating point numbers are not exact – this has boundaries, cannot use it for mathematical exactness

+ is overloaded – has different meanings depending on what it is applied to 
Means addition when applied to two numbers and concatenation when applied to two strings


* also overloaded – when applied to int and str it is a repetition integer

Type checking – produces errors when objects are not properly defined or the meaning of a line of code is ambiguous

e.g. ‘4’ < 3 in python 2 used to produce the answer false, the developers decided that any string is automatically larger than any numeric value. Now it produces an error message.

len function – length of a string 

### Indexing
Extracts individual characters from a string
0 is first number


### Slicing
slice(start, stop, step)
Mainly takes three parameters, used to slice a given sequence, e.g. extract characters from a given substring

### Input 
Takes a string as an argument and displays a prompt in the shell
Then waits for the user to type something
The line typed by the user is then treated as a string - becomes the value returned by the function
Need to specify otherwise if not to be treated as a string:
Input(‘Enter your name: ‘)

### Type conversions – use the name of a type to convert values to that type
Store a number in a string and then do calculations with it 
Int(string) tells the computer to treat it as a string 

### Iteration
Used when we want the program to do the same thing many times 
Also called looping
Begins with a test, if the test evaluates to be true, the program executes the loop body once, then goes back to re-evaluate the test
This process is repeated until the test evaluates to False, after which control passes to the code following the iteration statement.
Or it ends after it has looped through all the code?

Placing a break in the loop terminates the loop in which it is contained and transfers control to the code immediately following the loop.

A control flow statement executed repeatedly on a give Boolean condition
A repeating if statement

To create a count = 

Variable +1

Visit Finger Exercises to see a loop 

## Chapter 3 Lecture and Notes - Numerical Programs

!= 
Means not equal to 

### Exhaustive enumeration
### Decrementing function: Should think about this whenever you write a loop

Has the following properties – 
Maps a set of program variables into the integer
When the loop is entered, its value is nonnegative 
When its value is less than or equal to zero, the loop terminates
Its value is decreased every time through the loop

The decrementing function doesn’t have to be explicitly present, but the step is completed anyhow.

Function - block of code that takes in inputs and puts out outputs

repr – internal representation

Exhaustive enumeration – variant of guess a check, enumerate all possibilities until we get to the right answer or exhaust the space of possibilities.

For loops
for variable in sequence:
       code block

the variable following for is bound to the first value in the sequence, and the code block is executed.
The variable is then assigned the second value in the sequence, and the code is executed again
The process continues until the sequence is exhausted or a break statement is executed within the code block 

Built in function range: 
Takes three integer arguments: start, stop and step 


### Approximate Solutions and Bisection Search 

Cannot always find the square root, but can find an answer hat is close enough to the actual square root to be useful.
Close enough – is an answer that lies within some constant, epsilon?, of the actual answer

Error margin 
If the answer is good enough 
Sqrt 4 is 2 – but a computer might not always find that, epsilon is giving the computer the allowance to get it close enough

Number of iterations the computer does depends on how close the number is to zero, and the step size in iterations.
If the amount of iterations is too large the program might not find the answer as the step size is too large and it skipped over all the suitable answers.

Need to choose a different way to approach the problem than using exhaustive enumeration
Need to choose a better algorithm

Need to apply the same logic that is applied, in organising dictionaries – ordering the information so it is easier to find 
 Exploit the fact that numbers are totally ordered – each number has its place alongside the others, either higher or lower

Bisection search is starting in the middle, asking if this is two big or two small, then halving again and again until the number is found.

abs() in Python
The abs() function is used to return the absolute value of a number.
Syntax:

abs(number)

number : Can be integer, a floating point
number or a complex number
The abs() takes only one argument, a number whose absolute value is to be returned. The argument can be an integer, a floating point number or a complex number.

If the argument is an integer or floating point number, abs() returns the absolute value in integer or float.
In case of complex number, abs() returns only the magnitude part and that can also be a floating point number.

Max(,)
Chooses the higher value inside max

3.4 Using Floats
 Numbers of type float, reasonably good approximation of real numbers – but not all the time.

Why they don’t is to do with binary and how they are represented in the computer 
Numbers in a computer are represented in binary as it makes it easy to build hardware switches – devices that can be. In on of 2 states – on or off.

Non-integer numbers are represented differently using floating point – 
We represent the significant digits and exponents in binary rather than decimal and raise 2 rather than 10 to the exponent.

The number 0.1 
Is represented a 0011, -101 in binary
This is equivalent to 3/32 e.g. 0.09375 – not 0.1

This mostly causes problems with equality - ==
Almost always more appropriate to ask if two floating point values are close enough to each other, not whether they are identical 
Better two write: abs(x-y) < 0.0001 rather than x == y 

Also can cause rounding errors that accumulate overtime in some programs or balance out in others

### 3.5 Newton-Raphson


Can be used to find the real roots of many functions including polynomial with one variable
Polynomial = 3xsq + 2x + 3
A polynomial with one variable is either 0 or the sum of a finite number of nonzero terms

Exponent – the number you are raising the variable to a power by, how many times you are multiplying the variable by itself

If p is a polynomial and r is a real number, we will write p(r) to stand for the value of the polynomial when x=r.
A root of the polynomial p is a solution to the equation p=0
e.g. a problem of finding approximation to the square root of 24 can be formulated as finding x such that xsq – 24 = 0. (x is the sqrt)

First derivatives – expressing how the value of e.g. a function changes in respect to changes to the variable within it.

P(guess)/p’(guess)

() – parentheses = multiply

A better approximation than xsq – 24 = 0 is…
If a value is an approximation to a root of a polynomial then 
Guess – p(guess)/p’(guess)

e.g. polynomial cx2 + k 
first derivative 2cx

guess = cx2 + k(guess)/2cx(guess)

next guess:
guess – (guess2 – k)/2(guess)

successive approximation – what the while loop is required to do in the Newton-Raphson method





Code demos and lecture slides on git lab

Computer science is somehow about describing processes so they can be done mechanically (Imperative Knowledge).
Primitive types – integer, float, Boolean

Iteration with while and for loops
Can’t implement Herons method of doing something until its good enough.
Need a mechanism for repeating pieces of code execution.

While loops:
While Boolean Expression:
    code block

If this Boolean expression is true, run the code block, and rather than continuing go back to the start and repeat again, something in theboolean expression changes.

Can do a loop that is while something is wrong, keep looping until it is right. The leave.

Not all square roots can be represented exactly by computers


Coding examples on slides – run them, test you so you can see whats happening 

Loops with a counter
Not always necessary?

While True:
     Statement then inside the box

An expression that has to reduce to a Boolean expression

Point of the while – keep doing something over and over again, until something over and over again 

Bool(3)
Coersian
All numbers are true except 0 

For loops:
Used for iterating over a sequence of objects often integers
For variable in sequence:
Code block

Iterating over collection sequentially(tuples, lists, sets, etc.)

Range ( ) for creating sequences: 
Like having a counter but you don’t have to have one 

Tuple – vector of objects?

Counting – use a for loop not a while loop

Cartesian Product: creates a set out of multiple inputs

Menu for loop:

Look up truth table - wikipedia

Generate a truth table for De Morgan’s Laws:
For Boolean values a and b:


Bisection search – splitting search space in half each time 

Can we use this method to converge on square roots?

Newton-Raphson method for Square Roots

Special case of Newton’s method for finding roots of polynomials Algorithm:
If guess is an approximation of a root of a polynomial p, then: 

Slide


Functions 
Write a block of code assigned to a variable which can be called via that variable at a later date in your script.

Placing programs into functions creates structure
This creates abstraction, hiding details – e.g. the mechanics of how sq rt works

Think about what functions you are going to need first – craft of programming 

Makes it easy to improve different aspects of code 

Functions are also objects – what does this meannn

Scoping
Inside the block of a function,


When a function is called it gets its own local environment, it checks its first environments first

When a function returns, the local environment is left behind


Refractor our herons method code into a function – def sqrt(x):

Key arguments and defaults - 

Functions as means of abstraction

Recursion


## Chapter 4 Lecture and Notes - Functions, Scoping and Abstraction

### Functions
Def – reserved word that tell Python that a function is about to be defined
Sequence of names within the parentheses following the function name are the formal parameters of the function.
The formal parameters are bound to the actual parameters (often referred to as arguments) of the function invocation (also referred to as a function call).
Function has a special statement – return 
Only used within the body of the function
Parameters provide lambda abstraction – allowing you to manipulate no specific objects

### Scoping
Variables become locally bound inside functions

### Specifications
Writing test functions to help with debugging
Built-in function help – use with docstrings “””


### Modules
A module is a .py file containing python definitions and statements. Can then be called with import.
Lots of useful modules that come as part of the python library anyway

### Files
Access files through a file handle 

nameHandle = open (filename, w)

Instructs system to create a file and then the argument w says that you want to write to it 

nameHandle.close()  

make sure to close it after writing in it to avoid errors in the file

can then read from it 
 (code in Chapter 4 python practice)

### Recursion
Recursion is a tool a programmer can use to invoke a function call on itself.

A recursive function is a function that calls itself until a "base condition" is true, and then executes a stop

Recursive algorithm - if the current problem represents a simple case, solve it. If not, divide into subproblems and apply the same strategy to them.




## Chapter 5 Lecture and Notes - Structured Types, Mutability, and Higher-Order Functions

int and float – scalar types, no accessible internal structure

str – non-scalar, structured, one can use indexing to extract individual characters from a string and slicing to extract substrings

### Tuple:
Immutable ordered sequences of elements
Don’t all need to be characters unlike strings
Comma separated list of elements – (1, ‘two’, 3)


### Sequences and multiple assignment
If you know the length of a sequence you can extract individual elements by assigning them to variables
x, y = (3, 4)
a, b, c = xyz

### Ranges
Also immutable
Range(start, stop, step)
If only two arguments are supplied a step of 1 is used
Doesn’t have to take much space as its fully defined by its start, stop and step value

### Xrange
The only difference from range is that whereas range returns a list range returns an xrange object
It doesn't actually create a static list  but creates the values as you need them meaning that if you have a really gigantic range you'd like to generate a list for billions of numbers, xrange is the function to use


### Lists
List – ordered sequence of values where each value is identified by an index
Lists are written with square brackets not parentheses []
Important difference with lists compared to tuples – they are mutable (they can change after they’ve been created)

### Aliasing
You can end up with two distinct paths to the same list object 
If the variable binding a list is not included in a larger list for example, then adding to that original list will not affect the combined list.
 
### Append
list.append(anotherlist)
The original structure of the two lists is maintained

### Extend
list.extend(anotherlist)
mutates list1

list3 = list1 + list2
creates a new list 

### Cloning

list(yourlist)
 us this in for loops
for I in list(yourlist):
to stop the list mutating during the for loops

List Comprehension
When a new list is created from a list where each element had the same operation applied to it

### Functions as object
Higher order programming
Functions are first-class objects that can be treated like objects of any other type e.g. int or list

Using functions like abs or map within other functions for example is higher order programming.

### Lambda (reserved word)
Python supports the creation of anonymous functions using this word
e.g.
lambda x, y: x*y

L = []

for i in map(lambda x, y: x**y, [1 ,2 ,3 4], [3, 2, 1, 0])
        L.append(i)
print(L)        

### Strings, Tuples, Ranges and Lists
Lists can be built incrementally
With tuples, aliasing is never a problem

### Dictionaries
Objects of type dict are like lists except we index them using keys
Literals of type dict are enclosed in curly braces, and each element is written as a key followed by a colon followed by a value
MonthNumbers = {‘Jan” :1}
Dictionaries are mutable
Dictionaries are unordered and can only be accessed by their key
monthNumbers[5] will not refer to the value that was entered 6th, may either refer to an internal id or a key

## Chapter 6 Lecture and Notes - Testing and Debugging

Testing – process of running a program to try and ascertain whether or not it works as intended
Debugging – process of trying to fix a program that you already know does not work as intended

Key is breaking up the program into separate components that can be implemented, tested and debugged independently of the other components.

Testing
Black-box testing – are constructed without looking at the code to be tested
Test boundary conditions – adding in extremely large or small values for variables
Also need to consider aliasing in testing.

### Glass-box testing – shouldn’t skip black-box testing, but also need to look at the code to figure out what new tests will provide information.
A glass-box test suite is path-complete if it exercises every potential path through the program.
Impossible to achieve, usually.
Certain rules to follow: 
- exercise both branches of all if statements
- Make sure each except clause is executed
- for each for loop, have test cases for which: the loop is not entered, the body of the loop is executed exactly once, the body of the loop is executed more than once.
- for each while loop, do the same thing as for for loops, include test cases for all the possible ways of exiting the loops.

### Conducting Tests:

#### Unit testing – 
During this phase testers construct and run tests designed to ascertain whether individual units of code e.g. functions, work properly.

#### Integration testing – 
Whether the whole program as a whole behaves as intended.

#### Testers cycle through these two phases.
Integration testing is harder.
Many industrial software development organisations have a software quality assurance (SQA) group.

#### Testing is normally highly automated – 
Sets up the environment needed to invoke the program to be tested
Invoke the program to be tested with a predefined or automatically generated sequence of inputs
Save the results of the invocations
Check the acceptability of the results of the tests
Prepare an appropriate report

### Stubs – 
Simulate parts of the program being used by the unit being tested
Stubs are useful because they allow people to test units that depend upon software or hardware that doesn’t exist

Automated testing is good as it stops regression testing -when a programmer debugs a program by fixing something that breaks and, in the meantime, breaking something that works.

### Debugging – 
If your program has a bug it is because you put it there, if your program has multiple bugs it is because you made multiple mistakes.

Run time bug characterisations:

Overt -> covert: an overt bug has an obvious manifestation, a covert bug does not.
Persistent -> intermittent: Persistent bug occurs every time the program runs, an intermittent bug only occurs some of the time.

## Chapter 7 Notes and Lecture - Exceptions and Assertions

### Last time:
List comprehensions - a concise way if applying a function to the values in a sequence, producing a new list of the results
Generator expressions - similar syntax but return objects instead of lists

Introduced Python dictionaries (type dict):
- Dicts are a set of key/value pairs
- where keys can be any immutable object that supports hash()
- and values can be any object
- including nested objects

Dicts supports deletion, insertion and iteration in near constant time 

Programming Errors
Syntax errors - grammatically incorrect code, caught at compile time e.g. before the program starts running
Run-time errors - 

### Exceptions
Don’t have to be fatal runtime errors
Can be caught and handled
An exception is a signal that an error or other unusual condition has occurred.

### Try/except blocks:
Try:
	you do your operations here
Except Exception1:
	if there is exception 1, then execute this block
Except Exception2:
	if there is exception2, then execute this block

### 8 or so common exceptions
Have to know the name of the error you are going to get to catch them 
Can find this out by causing an error and seeing what the name of that is 
Also a way of catching all exceptions - has issues

### Built-In Exceptions:
AssertionError - Assert statement fails
IndexError - Index of a sequence is out of range
ImportError - Module not found
KeyError - specified key is not found in dictionary
NameError - identifier is not found in the global or local namespace
TypeError - function or operation is applied to an object of incorrect type
ValueError - function gets arguments of correct type but improper value

### Exceptions as Control Flow Mechanisms
Not purely for errors
Convenient flow-of-control mechanism that can be used to simplify programs

In Python it is more usual to have a function raise an exception when it cannot produce a result that is consistent with the functions specification.


### Throwing exception with raise():
Construct new exception object with message
Can pass failure data conveniently back to shallower caller across nested function calls

Raise has two purposes - 
Its used for raising your own errors, 

If something:
	raise Exception(‘My error!’)

Or to reraise the current exception and an exception handler so that it can be handled further up the call stack:
try:
	generate_exception()
Except SomeException as e:
	if not can_handle(e)
		raise
	handle_exception(e)




### Assertions
Asserts should be used to test conditions that should never happen, the purpose is to crash early in case of a corrupt program state.
Exceptions should be used for errors that can concievably happen. 

Assert boolean (throws AssertionError)
Carried out by the Assert statement
An expression is tested and if the result come up false, an exception is raised
Can turn off when you are done testing your program
Useful defensive programming tool, can be used to confirm that the arguments to a function are of appropriate types



## Chapter 8 Lecture and Notes: Classes and Object Oriented Programming:

### Objects
Data types - integer (1 and 3 are objects)
Strings are objects
Tuples are a way of combining objects

Every object has a type, and that defines what can be done with that object, the objects behaviours.
len - the knowledge of the length of an object lives inside the object itself

### Abstract Data Type - 
An ADT is a set of objects and the operations on those objects
These operations define an interface, an abstraction.
What interface does type list present?

### Namespaces 
A mapping from names to objects
The set of attributes of an object also for a namespace
Attribute - any name following a . 
Namespaces work the same as scopes and do not cause conflict outside of their module
In the expression z.real, real is an attribute of the object


### Modules 
A module is a file consisting of Python code. A module can define functions, classes and variables. A module can also include runnable code

Create a class for rational numbers
Methods - how rational numbers add together 

### Classes vs Instances
A class is a blueprint for creating instances, for example, for a class which defines employees, each employee of that class is an instance of it.

Classes are Pythons mechanism for making new types
We can define these ourselves and methods to our class that create the behaviours we want to produce
A tuple is a type, we can create our own

In practice, the statements inside a class definition will usually be function definitions, but other statements are allowed, and sometimes useful.

Class objects support two kinds of operations: attribute references and instantiation.

Attribute references use the standard syntax used for all attribute references in Python: obj.name. 

Methods = a function that is associated with a class

### Class Methods and Static Methods
In a class, methods take the instance as the first argument, called self.
Can change this so that the method takes a class as the first method.
Use @classmethod decorator for this

### Inheritance - Creating Subclasses
Method resolution order - if there is nothing in the subclass, python starts looking up the classes for information
Help function - run on the subclasses, shows you the method resolution order, the places where python searches for attributes and methods
Allows you to custom change methods for the subclasses
Ypu never want to pass mutable data types like a list or a dictionary as default arguments for functions or classes

### self - 

Instance and object - the same.
Self is referring to the instance.
Creating a new class creates a new type of object, allowing new instances of that type to be made.

If you call it a string in the class then when python calls it it will look inside the method to see if it can find out what it is and return a string


### Special Methods - 
__ = dunder
Classes - Special Method names
__something__

__init__ is a special method

should always implement - 
def __repr__(self):
	Used for when we call repr(instance1) on one of our objects
	Used for debugging by other developers
	Repr can be used as a minimum if there is not __str__ also 
	Create something that you can put back into Python code and recreate that same object
	formatting outputs - e.g. for datetime

def __str__ (self):
	Used for when we call str(instance1) on one of our objects
	Readable representation of an object
	Changing how your classes are printed and displayed 

def __add__ (self):
	changes the ways add works on the objects 

def __len__ (self):
	return length of object variables


## Decorators 
@property
If you make a new method for a more complicated attribute, you can use a decorator so that the method can be accessed as though it is a normal attribute.

@setter and @getter
setter for changing the data
fetter for retrieving the data

@delete 
can then use del instance.attribute to delete an object instance


### Summary:
Instance methods are like functions with a special first parameter (conventionally called self). When we call an instance method through an object using dot notation, the object is passed to the instance method as the first formal parameter.
Classes have a special method named __init__ which creates new objects. We don't call it directly, Python invokes it when we call Classname(...). Python creates a new object of type Classname, the calls it's __init__ method.
We can provide our class with instance methods with special names that means that it then works seamlessly with built in Python functions.

### Nota Bene:
By creating new types with classes, we are "hiding" the way the object is represented. This is called data abstraction.
The hiding is by convention, not enforced:
a = Rational(1, 2)
a.denominator (instance variable) is visible, but referencing it is discouraged. Why? You can reassign to variables, might not follow all the rules.


### Class Variables vs Instance Variables:

Class variables are variables that are shared by all instances of a class

Can have attributes outside of class functions which are assigned all of the instances.
Functions in the class then add parameters for the variables which change per instance of the class.
Shouldn't put anything that has a side effect(output?) e.g. printing in a class structure.

Can derive a class from a base class object
The most derived class wins

Format is an instance method of type string
We are passing a tuple to format

Def __str__(self):
	return "[0][1]".format(

Write functions that take an arbitrary number of arguments as long as you manipulate through tuple
**
* 
Before function arguments

### Inheritance
Class object is at the top of the hierarchy of inheritance

#### Isinstance
Tells you what the type of an object is, built into python
First argument can be any object, second argument must be an object of type type
Boolean - returns true if the object is of the type entered


## Chapter 9 Lecture - Intro Algorithmic Complexity and Big O notation

Understanding the efficiency of our programs:
Should care that they are efficient, data sets can be very large (google indexes 130 trillion), how to index and search.
Users care about performance. Amazon experimented with adding 100ms delays to their shopping cart checkout conversion fell measurably/
If your algorithm is more efficient it costs less electricity to run.

3 ways of comparing runtime efficiency - 
be scientists, measure time
- measuring small size problems doesn't necessarily predict what happens when the product size gets large

count operations

abstract idea of order of growth
Express efficiency in terms of size of input

When we double input size - 
may remain constant if its one mathematical operation
quadratic time for nested loops
logarithmic
linear
exponential

Two linears (nested loops) make a quadratic

Can reorganise the code so it become a constant and can deal with larger problems 

Using a dict - happens in near constant time


## Big O notation
Lets us characterise functions according to their growth rates/how long a program takes to run
Expressing run time in terms of - how quickly it grows relative to the input as the input get arbitrarily large.
Used in Computer Science to describe the performance or complexity of an algorithm
Big O is the worst-case scenario, can be used to describe the execution time required or the space used


O(1)
Describes and algorithm that always execute in the same time or space regardless of the size of the input dataset
Example of constant = boolean statement

O(N)
Describes an algorithm whose performance will grow linearly and in direct proportion to the size of the the input data set.
Example of a worse case scenario - during the iteration of the for loop it could be returned early but Big O notation always assumes the maximum number of iterations
Example of linearly = for loops

O(N^2) 
Represents an algorithm whose performance is directly proportional to the square of the size of the input data set 
Common with algorithms that involve nested iterations over the data set
Quadratic, polynomial
Tend to occur in nested loops

O(2^N) 
Denotes an algorithm whose growth doubles with each addition to the input data set 
Exponential
Fibonacci, Power Set

With big 0 we want to think about moving up the hierarchy from exponential to constant for all our code 

if key in dict
Different from 
If element in list

If element in list requires loop - can't jump to key

The way to move up the hierarchy of Big O Notation is to start with a brute force solution, look for repeat work in that solution, and modify it to do only that work once.

Binary, bisection research - 

## Complexity of Common Python Functions

Dictionaries have a worst case that can be a linear search rather than constant time

# Chapter 10 Some Simple Algorithms and Data Structures

The key to efficiency is a good algorithm

Key to successful programming - 
- Develop an understanding of the inherent complexity of the problem
- Think about how to break that problem down into subproblems
- Relate those subproblems to other problems for which efficient algorithms already exist

Really efficient programs often really hard to understand, should solve the problem at hand in the most straightforward way possible, instrument it to find computational bottlenecks and lock for ways to improve to the computational complexity to solve these bottle necks.

## Search Algorithms - 
Finding an item with specific properties from a selection of items within the search space
Exhaustive enumeration, bisection search, Newton-Raphson = all search algorithms

### Linear Search and Using Indirection to Access Elements
for i in range(len(L)):
	if L[i] == e:
		return True
return False

At best linear - only if each operation inside the loop can be done in constant time. 
A list in python is represented as a length (the number of objects in a list) and a sequence of fixed-size pointers to objects.
This pointer stays the same regardless of the size of the actual object.
This means a list of n length will always be the same size in memory as another list of n length.
This address can be found in constant time.
And the value/object stored at this address can also be found in constant time.

This technique, indirection, is important in computing, it involves accessing something that contains a reference to the thing they initially sought.
This is what happens when we use a variable as well.

### Binary Search and Exploiting Assumptions
Can get a considerable improvement on the worst-case scenario of linear search by using binary search
Most simple version of binary search uses recursion

We know that the program terminates becuas e it has a decrementing function of high - low

Algorithmic complexity of binary search - 0(log(len(L)))

## Sorting Algorithms
Is sorting complexity less that 0(len(L))? - because if it is then we should sort a list before we search it
It is not, the complexity will always be at least linear becuase you can't sort a list without looking at each element in the list
However, if we plan to search the list many times, then sorting it first make be less complex
Sorting in python runs in 0(n*log(n)) time, where n is the length of the list

#### Selection sort
Loop invariants can be reasoned with induction:
Base case: at the start of the first iteration the frefix is empty, the suffix is the entire list, therefore the invariant true
Induction step: at each step of the algorithm, we move one element from the suffix to the prefix, always move the smallest element from the suffix to the prefix, no element in the prefix is larger than an element in the suffix.
Termination: wehen the loop is exited, the prefix includes the entire list
This is the most simple but most inefficient sort
Quadratic in the length of L - 0(len(L)^2)

Selection sort example in book 

#### Merge sort
Divide and conquer algorithm
Characterised by:
A threshold input size, below which the problem is not subdivided
The size and number of sub-instances into which an instance is split
The algorithm used to combine sub-solutions

Threshold = recursive base

Recursive merge sort - 
1. If the list is of length 0 or 1, it is already sorted
2. If the list has more than one element, split the list into two lists and use merge sort to sort each of them 
3. Merge the results

Merge sort example in book

Merging two lists is linear to the length of the lists

Selection sort has a  memory price, it works by swapping th eplace of elements in the list, uses a constant amount of extra storage.

###  Sorting in Python
Sorting algorithm used in most python implementations = timsort
key idea is to take advantage of the fact that alot of data is alreayd partially osrted
Timsorts worst case is the same as merge sorts, but on average its alot better

### Hash Tables
Hash function, like the hashtables used in a dictionary, where the keys are already and index of integers.
A hash function maps a large space of inputs (e.g. all natural numbers) 









