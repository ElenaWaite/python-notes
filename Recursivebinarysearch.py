#wrapper function, abstraction for implementation, low and high have nothing to do with searching a list
def search(L, e):
    def bSearch(L, e, low, high):
        #Decrements high - low
        if high == low:
            return L[low] == e
        mid = (low + high) // 2
        if L[mid] == e:
            return True
        elif L[mid] > e:
            if low == mid: #nothing left to search
                return False
            else:
                return bSearch(L, e, low, mid - 1)
        else:
            return bSearch(L, e, mid + 1, high)
    if len(L) == 0:
        return False
    else:
        return bSearch(L, e, 0, len(L) - 1)

#if you search for for an int in a list which is only strings, it returns an error
list1 = ['Hello', 'my', 'name', 'is', 'something']
object1 = 'Hello'
print(search(list1, object1))